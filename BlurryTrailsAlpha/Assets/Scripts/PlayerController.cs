using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public GameObject myLightGO;
    public GameObject myRangeGO;
    Light myLightProperty;
    AudioSource mySource;
    

    public Transform maxTarget;
    public Transform minTarget;
    public float speed;

    float cooldown = 1.5f;
    float lastPressed;

    bool canMoveLight = true;


    void Start()
    {
        myLightProperty = myLightGO.GetComponent<Light>();
        mySource = GetComponent<AudioSource>();
         
    }

 
    void Update()
    {
        checkKeys();
        checkCooldown();
    }

   

    void checkKeys() 
    {
        if (Input.GetKeyDown(KeyCode.Space) && canMoveLight == true)
        {
            canMoveLight = false;
            mySource.Play();
            Vector3 a = myLightGO.transform.position;
            Vector3 b = maxTarget.position;

            myLightGO.transform.position = Vector3.Lerp(a, b, speed * Time.deltaTime);
            myLightProperty.range = 12;
            myLightProperty.intensity = 9;

            myRangeGO.transform.localScale = new Vector3(4, 0.1f, 4.20f);
            
        }
    
    }

    void checkCooldown() 
    {
        if (Time.time - lastPressed<cooldown)
        {
            return;
        }
        else
        {
            goBackDown();        
        }
        lastPressed = Time.time;
    }

    void goBackDown() 
    {
        Vector3 a = myLightGO.transform.position;
        Vector3 b = minTarget.position;

        myLightGO.transform.position = Vector3.Lerp(a, b, speed * Time.deltaTime);
        myLightProperty.range = 2.55f;
        myLightProperty.intensity = 3.07f;

        myRangeGO.transform.localScale = new Vector3(1.04f, 0.1f, 1.04f);
        canMoveLight = true;
    }

}
