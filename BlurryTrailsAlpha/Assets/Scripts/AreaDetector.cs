using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDetector : MonoBehaviour
{

    //BoxCollider myCollider;
    Renderer GORenderer;
    public Material[] materialsArray;
    public List<Renderer> renderList = new List<Renderer>();

    public GameObject particlesGO;
    ParticleSystem childParticles;
    public GameObject playerGO;
    MeshRenderer playerMesh;

    float cooldown = 3;
    float lastPressed;

  

    void Start()
    {
        // myCollider = this.GetComponent<BoxCollider>();
        childParticles = particlesGO.GetComponent<ParticleSystem>();
        playerMesh = playerGO.GetComponent<MeshRenderer>();

    }

    int i = 0;
    private void OnTriggerEnter(Collider other)
    {     
        if (other.gameObject.tag == "Obstacle") 
        {
            GORenderer = other.GetComponent<Renderer>();
            renderList.Add(GORenderer);
            i++;
            GORenderer.material = materialsArray[0];
           
        }
        else if (other.gameObject.tag == "FinishLine")
        {
            childParticles.Play();
            playerMesh.enabled = false;
            
        }
    }

    void Update()
    {
        checkCooldown();
    }

    void checkCooldown()
    {
        if (Time.time - lastPressed < cooldown)
        {
            return;
        }
        else
        {
            changeColorBack();
        }
        lastPressed = Time.time;
    }

    void changeColorBack()
    {
        foreach (Renderer item in renderList)
        {
            item.material = materialsArray[1];
            
        }
        renderList.Clear();
    }


}
