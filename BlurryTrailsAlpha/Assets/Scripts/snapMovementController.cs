using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class snapMovementController : MonoBehaviour
{
    Vector3 up = Vector3.zero,
    right = new Vector3(0, 90, 0),
    down = new Vector3(0, 180, 0),
    left = new Vector3(0, 270, 0),
    currentDir = Vector3.zero;

    Vector3 nextPos, destination, aimedDir;

    public float speed = 3.5f;
    float rayLength = 1f;

    bool canMove;

    void Start()
    {
        currentDir = up;
        nextPos = Vector3.forward;
        destination = transform.position;
    }

 
    void Update()
    {
        Move();

    }


    void Move() 
    {

        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) 
        {
            nextPos = Vector3.forward;
            currentDir = up;
            canMove = true;
        }

        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            nextPos = Vector3.back;
            currentDir = down;
            canMove = true;
        }

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            nextPos = Vector3.right;
            currentDir = right;
            canMove = true;
        }

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            nextPos = Vector3.left;
            currentDir = left;
            canMove = true;
        }


        if (Vector3.Distance(destination,transform.position) <=0.00001f)
        {
            transform.localEulerAngles = currentDir;
            if (canMove)
            {
                if (CheckValid())
                {
                    destination = transform.position + nextPos;
                    currentDir = nextPos;
                    canMove = false;
                }
                
            }
            
        }
    }

    bool CheckValid() 
    {
        Ray myRay = new Ray(transform.position + new Vector3(0, 0.25f, 0),transform.forward);
        RaycastHit hit;

        Debug.DrawRay(myRay.origin, myRay.direction, Color.red);

        if (Physics.Raycast(myRay,out hit,rayLength))
        {
            if (hit.collider.tag == "Obstacle")
            {
                return false;
            }
        }
        return true;
    }
}
